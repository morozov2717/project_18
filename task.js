const http = require('http');
const fs = require('fs');
const path = require('path');

const PORT = 3200;

const data = fs.readFileSync(path.resolve(__dirname, 'public', 'index.html'));    
const img = fs.readFileSync(path.resolve(__dirname, 'public', 'nodejs.jpg'));

const server = http.createServer((req, res)=>{
    switch(req.url){
        case '/html': {
            console.log(req.url);
            res.setHeader("Content-Type", "text/html");
            res.writeHead(200);
            res.end(data);
            break;
        }
        case '/img':{
            console.log(req.url);
            res.setHeader("Content-Type", "image/jpeg");
            res.writeHead(200);
            res.end(img);
            break;
        }
        default:
            res.writeHead(404);
            res.end("Resource not found");
    }
});

server.listen(PORT, 'localhost', (err)=>{
    err ? console.log(err) : console.log(`listening port ${PORT}`);
});
